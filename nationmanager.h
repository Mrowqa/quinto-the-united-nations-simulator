#ifndef NATIONMANAGER_H
#define NATIONMANAGER_H

#include "nationlist.h"
#include "nationvoting.h"
#include "nationsession.h"
#include <memory>

class NationManager : public NationList
{
public:
    NationSession& getSession() const { if(!session) throw std::logic_error("NationManager::getSession() -- no lasting session"); return *session; }
    inline bool isIdle() const;
    bool startSession();
    bool endSession();
    bool isActivedSession() const       { return static_cast<bool>(session); }
    std::size_t getStartedSessionsCount() const         { return sessionsCounter; }
    std::size_t getAllVotingsCount() const              { return votingsCounter + getCurrentSessionVotingsCount(); }
    std::size_t getCurrentSessionVotingsCount() const   { return (session ? session->getVotingsCount() : 0); }
    NationSession::Result getResult() const             { return result + (session ? session->getResult() : NationSession::Result()); }
    void resetStatistics()              { result.reset(); sessionsCounter = votingsCounter = 0; }

    NationManager() = default;

private:
    std::unique_ptr<NationSession> session;
    NationSession::Result result;
    std::size_t sessionsCounter = 0;
    std::size_t votingsCounter = 0;
};

#endif // NATIONMANAGER_H
