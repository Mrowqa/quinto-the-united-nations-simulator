#ifndef NATION_H
#define NATION_H

#include <string>
#include <QIcon>

class Nation
{
public:
    const std::string name;
    const bool canVeto;
    const QIcon flag;

    Nation(std::string name="", bool canVeto=false, std::string flagImageFilename="")
        : name(name), canVeto(canVeto), flag((flagImageFilename.empty() ? defaultFlagPath : flagImageFilename).c_str()) {}

    static const std::string defaultFlagPath;
};

#endif // NATION_H
