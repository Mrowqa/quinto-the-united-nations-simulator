#ifndef DIALOGSTATISTICS_H
#define DIALOGSTATISTICS_H

#include <QDialog>
#include "nationmanager.h"

namespace Ui {
class DialogStatistics;
}

class DialogStatistics : public QDialog
{
    Q_OBJECT

public:
    explicit DialogStatistics(QWidget *parent = 0);
    void setInfo(const NationManager& nManager);
    ~DialogStatistics();

private:
    Ui::DialogStatistics *ui;
};

#endif // DIALOGSTATISTICS_H
