#ifndef NATIONVOTING_H
#define NATIONVOTING_H

#include <vector>
#include <cassert>

#define UINT(n) (static_cast<std::size_t>((n)))
class NationSession;

class NationVoting
{
public:
    enum class Vote {
        InFavour,
        Against,
        Abstain,
        AllVotes
    };

    struct Result {
        enum class Type {
            VotingLasting,
            AllResults = VotingLasting,
            Success,
            Failure,
            FailureVeto,
            Undecided
        } state = Type::VotingLasting;

        std::size_t operator[](NationVoting::Vote voteType) const
        {
            return voteType == NationVoting::Vote::AllVotes ?
                    votes[UINT(NationVoting::Vote::InFavour)] + votes[UINT(NationVoting::Vote::Against)] + votes[UINT(NationVoting::Vote::Abstain)]
                    : votes[UINT(voteType)];
        }

    private:
        std::size_t votes[4] = {0, 0, 0, 0};
        friend class NationVoting;
    };


    bool vote(std::size_t nationNumber, Vote vote);
    bool isEnded() const            { return result.state != Result::Type::VotingLasting; }
    const Result& getResult() const { return result; }

    NationVoting(const NationSession& nSession);
    ~NationVoting() { /*assert(isEnded());*/ }

private:
    const NationSession& session;
    std::vector<Vote> votes;
    Result result;
};


#endif // NATIONVOTING_H
