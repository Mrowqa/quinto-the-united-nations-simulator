#ifndef NATIONLIST_H
#define NATIONLIST_H

#include "nation.h"
#include <vector>
#include <cassert>

class NationList
{
public:
    bool loadListFromFile(std::string listFilename);
    std::size_t getCount() const                    { return nationList.size(); }
    const Nation& operator[](std::size_t no) const  { assert(no < nationList.size()); return nationList[no]; }
    std::size_t locksAbleToDelete = 0;

    NationList() = default;
    ~NationList() = default;
    NationList(std::string listFilename)    { loadListFromFile(listFilename); }

private:
    std::vector<Nation> nationList;
    mutable std::size_t locks = 0;
    friend class NationSession;
};

#endif // NATIONLIST_H
