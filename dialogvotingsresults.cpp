#include "dialogvotingsresults.h"
#include "ui_dialogvotingsresults.h"

DialogVotingsResults::DialogVotingsResults(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogVotingsResults)
{
    ui->setupUi(this);
}

void DialogVotingsResults::setInfo(const NationManager &nManager)
{
    NationVoting::Result votingResult = nManager.getSession().getLastVotingResult();
    std::size_t inFavourCnt = votingResult[NationVoting::Vote::InFavour];
    std::size_t againstCnt  = votingResult[NationVoting::Vote::Against];
    std::size_t abstainCnt  = votingResult[NationVoting::Vote::Abstain];
    std::size_t allCount    = votingResult[NationVoting::Vote::AllVotes];

    double inFavourPercent  = 100.0 * inFavourCnt / allCount;
    double againstPercent   = 100.0 * againstCnt / allCount;
    double abstainPercent   = 100.0 * abstainCnt / allCount;

    QString title, result;
    title = "[Session " + QString::number(nManager.getStartedSessionsCount()) + "] Voting #" + QString::number(nManager.getAllVotingsCount()+1);
    this->setWindowTitle(title);

    if(votingResult.state == NationVoting::Result::Type::FailureVeto)
    {
        result = "Veto!";
        QLayoutItem *item = NULL;
        while ((item = ui->resultContainer->takeAt(0)) != 0)
            delete item->widget();
    }
    else
    {
        switch(votingResult.state)
        {
        case NationVoting::Result::Type::Success:
            result = "SUCCESS!";
            break;

        case NationVoting::Result::Type::Undecided:
            result = "Undecided voting!";
            break;

        case NationVoting::Result::Type::Failure:
            result = "FAILURE!";
            break;

        default:
            result = "Unknown state.";
            break;
        }
        ui->inFavourValue->setText(QString::number(inFavourCnt) + " (" + QString::number(inFavourPercent, 'f', 2) + "%)");
        ui->againstValue->setText(QString::number(againstCnt)  + " (" + QString::number(againstPercent, 'f', 2)  + "%)");
        ui->abstainsValue->setText(QString::number(abstainCnt)  + " (" + QString::number(abstainPercent, 'f', 2)  + "%)");
    }
    ui->resultLabel->setText(result);
}

DialogVotingsResults::~DialogVotingsResults()
{
    delete ui;
}
