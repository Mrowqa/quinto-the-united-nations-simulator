#ifndef NATIONSESSION_H
#define NATIONSESSION_H

#include "nationvoting.h"
#include <memory>
#include <map>

#define UINT(n) (static_cast<std::size_t>((n)))
class NationManager;

class NationSession
{
public:
    enum class PresenceState {
        None,
        Present,
        PresentAndVoting,
        Absent
    };

    struct Result {
        std::size_t operator[](NationVoting::Result::Type resType) const;
        Result& operator+=(const Result& other);
        Result  operator+ (const Result& other) const;
        void reset()    { for(std::size_t i=0; i<resLen; i++) results[i] = 0; }

    private:
        std::size_t results[5] = { 0, 0, 0, 0, 0 };
        static const std::size_t resLen = sizeof(results)/sizeof(std::size_t);
        friend class NationManager;
        friend class NationSession;
    };

    bool setPresenceState(std::size_t nationNumber, PresenceState state);
    PresenceState getPresenceState(std::size_t nationNumber) const      { assert(nationNumber < presenceSheet.size()); return presenceSheet[nationNumber]; }
    std::vector<std::size_t> getRegistered() const; // r-reference with std::move dont works under debugger :/
    bool isRegistered(std::size_t nationNumber) const                   { assert(nationNumber < presenceSheet.size()); return presenceSheet[nationNumber]==PresenceState::Present || presenceSheet[nationNumber]==PresenceState::PresentAndVoting; }
    std::size_t getRegisteredCount() const  { return registeredCount; }
    NationVoting& getVoting() const         { if(!voting) throw std::logic_error("NationVoting::getVoting() -- no lasting voting"); return *voting; }
    std::size_t getVotingsCount()           { endVoting(); return votingsCounter; }
    bool isIdle() const         { return checkedCount==presenceSheet.size() && (!voting || voting->isEnded()); }
    bool startVoting()          { endVoting(); if(voting) return false; voting.reset(new NationVoting(*this)); return true; }
    bool endVoting()            { if(!voting || !voting->isEnded()) return false; getLastVotingResult(); result.results[UINT(voting->getResult().state)]++; votingsCounter++; voting.release(); return true; }
    const Result& getResult()   { endVoting(); return result; }
    const NationVoting::Result& getLastVotingResult()                   { if(voting) lastVotingResult = voting->getResult(); return lastVotingResult; }

    NationSession(const NationManager& nManager);
    ~NationSession();

private:
    const NationManager& nationManager;
    std::vector<PresenceState> presenceSheet;
    std::size_t registeredCount = 0;
    std::size_t checkedCount = 0;
    std::unique_ptr<NationVoting> voting; // mutable?
    Result result;
    NationVoting::Result lastVotingResult;
    std::size_t votingsCounter = 0;
    friend class NationVoting;
};


#endif // NATIONSESSION_H
