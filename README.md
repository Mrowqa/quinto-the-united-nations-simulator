Quinto is simply application for simulating UN votings. I've written it for [Olsztyn Model United Nations](http://olmun.com/).

Application is written in C++ using Qt framework. You can read about development process of this application on [my blog](http://mrowqa.pl/?p=665) (polish only).