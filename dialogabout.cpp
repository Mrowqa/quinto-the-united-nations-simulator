#include "dialogabout.h"
#include "ui_dialogabout.h"
#include "mainwindow.h"

DialogAbout::DialogAbout(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogAbout)
{
    ui->setupUi(this);
    ui->versionText->setText("Quinto " + QString(QUINTO_VERSION));
}

DialogAbout::~DialogAbout()
{
    delete ui;
}
