#include "nationlist.h"
#include <QFile>
#include <QTextStream>
#include <QRegExp>
#include <cassert>

bool NationList::loadListFromFile(std::string listFilename)
{
    if(locks > locksAbleToDelete)
        return false;

    QFile file(listFilename.c_str());
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    std::size_t lastSlash = listFilename.find_last_of("/\\");
    if(lastSlash==std::string::npos) lastSlash = 0;
    std::string listDirectory(listFilename, 0, lastSlash);
    listDirectory += listDirectory.find('\\') == std::string::npos ? '/' : '\\';

    /*
     * File format (each line):
     * +[vn] Nation Name | *Relative* path to flag image from list file
     */

    std::vector<Nation> tmpNationList;
    QTextStream in(&file);
    while(!in.atEnd())
    {
        QString line = in.readLine();
        QRegExp rxCheck("^\\+([vn]) ([^\\|]*)( \\| (.*))?$");
        if(!rxCheck.exactMatch(line))
            return false;

        bool canVeto = rxCheck.cap(1) == "v";
        std::string name = rxCheck.cap(2).toStdString();
        std::string flagImageFilename = rxCheck.cap(4).toStdString();

        if(!flagImageFilename.empty())
            flagImageFilename = listDirectory + flagImageFilename;

        tmpNationList.push_back(Nation(name, canVeto, flagImageFilename));
    }
    file.close();
    if(tmpNationList.empty())
        return false;
    nationList.swap(tmpNationList);

    return true;
}
