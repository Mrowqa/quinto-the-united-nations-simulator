#-------------------------------------------------
#
# Project created by QtCreator 2013-09-12T20:11:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = quinto
TEMPLATE = app
CONFIG += c++11


SOURCES += main.cpp\
        mainwindow.cpp \
    nationmanager.cpp \
    nationlist.cpp \
    nationsession.cpp \
    nationvoting.cpp \
    nation.cpp \
    dialogstatistics.cpp \
    dialogvotingsresults.cpp \
    dialogabout.cpp

HEADERS  += mainwindow.h \
    nation.h \
    nationmanager.h \
    nationlist.h \
    nationsession.h \
    nationvoting.h \
    dialogstatistics.h \
    dialogvotingsresults.h \
    dialogabout.h

FORMS    += mainwindow.ui \
    dialogstatistics.ui \
    dialogvotingsresults.ui \
    dialogabout.ui

RESOURCES += \
    toolbar_icons.qrc
