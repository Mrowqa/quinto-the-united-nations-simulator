#include "nationsession.h"
#include "nationvoting.h"
#include "nationmanager.h"

NationSession::NationSession(const NationManager& nManager) : nationManager(nManager)
{
    assert(nationManager.getCount());

    nationManager.locks++;
    presenceSheet.clear();
    presenceSheet.resize(nationManager.getCount(), PresenceState::None);
}

NationSession::~NationSession()
{
    //assert(isIdle());
    nationManager.locks--;
}

bool NationSession::setPresenceState(std::size_t nationNumber, PresenceState state)
{
    assert(nationNumber < presenceSheet.size());
    assert(state != PresenceState::None);

    if(presenceSheet[nationNumber] != PresenceState::None) return false;
    if(state != PresenceState::Absent) registeredCount++;
    presenceSheet[nationNumber] = state;
    checkedCount++;

    return true;
}

std::vector<std::size_t> NationSession::getRegistered() const
{
    std::vector<std::size_t> registeredItems;
    for(std::size_t i=0; i<presenceSheet.size(); i++)
        if(presenceSheet[i]==PresenceState::Present || presenceSheet[i]==PresenceState::PresentAndVoting)
            registeredItems.push_back(i);
    return registeredItems;
}

std::size_t NationSession::Result::operator[](NationVoting::Result::Type resType) const
{
    return resType == NationVoting::Result::Type::AllResults ?
                results[UINT(NationVoting::Result::Type::Success)] + results[UINT(NationVoting::Result::Type::Failure)]
            + results[UINT(NationVoting::Result::Type::FailureVeto)] + results[UINT(NationVoting::Result::Type::Undecided)]
            : results[UINT(resType)];
}

NationSession::Result &NationSession::Result::operator+=(const NationSession::Result &other)
{
    return *this = *this + other;
}

NationSession::Result NationSession::Result::operator+(const NationSession::Result &other) const
{
    Result tmp = *this;
    for(std::size_t i=0; i<resLen; i++)
        tmp.results[i] += other.results[i];
    return tmp;
}
