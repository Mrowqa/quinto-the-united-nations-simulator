#include "nationmanager.h"
#include "nationsession.h"

bool NationManager::isIdle() const
{
    return !session || session->isIdle();
}

bool NationManager::startSession()
{
    endSession();
    if(!isIdle())
        return false;
    session.reset(new NationSession(*this));
    sessionsCounter++;
    return true;
}

bool NationManager::endSession()
{
    if(!isIdle() || !session)
        return false;
    result += session->getResult();
    votingsCounter += session->getVotingsCount();
    session.release();
    return true;
}
