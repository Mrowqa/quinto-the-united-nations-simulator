#include "dialogstatistics.h"
#include "ui_dialogstatistics.h"

DialogStatistics::DialogStatistics(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogStatistics)
{
    ui->setupUi(this);
}

void DialogStatistics::setInfo(const NationManager &nManager)
{

    NationSession::Result result = nManager.getResult();

    std::size_t successCnt       = result[NationVoting::Result::Type::Success];
    std::size_t failureCnt       = result[NationVoting::Result::Type::Failure];
    std::size_t failureByVetoCnt = result[NationVoting::Result::Type::FailureVeto];
    std::size_t undecidedCnt     = result[NationVoting::Result::Type::Undecided];
    std::size_t allCount         = result[NationVoting::Result::Type::AllResults];

    double successPercent       = 100.0 * successCnt / allCount;
    double failurePercent       = 100.0 * (failureCnt + failureByVetoCnt) / allCount;
    double failureByVetoPercent = 100.0 * failureByVetoCnt / allCount;
    double undecidedPercent     = 100.0 * undecidedCnt / allCount;

    QString votingsCount;
    if(allCount == 0)
    {
        votingsCount = "No passed votings!";
        QLayoutItem *item = NULL;
        while ((item = ui->resultContainer->takeAt(0)) != 0)
            delete item->widget();
    }
    else
    {
        votingsCount = "from " + QString::number(nManager.getStartedSessionsCount()) + " session with " + QString::number(nManager.getAllVotingsCount()) + " votings";
        ui->successValue->setText(      QString::number(successCnt)                  + " (" + QString::number(successPercent, 'f', 2) + "%)");
        ui->failureValue->setText(      QString::number(failureCnt+failureByVetoCnt) + " (" + QString::number(failurePercent, 'f', 2) + "%)");
        ui->failureVetoValue->setText(  QString::number(failureByVetoCnt)            + " (" + QString::number(failureByVetoPercent, 'f', 2) + "%)");
        ui->undecidedValue->setText(    QString::number(undecidedCnt)                + " (" + QString::number(undecidedPercent, 'f', 2) + "%)");
    }
    ui->votingsCountInfo->setText(votingsCount);
}

DialogStatistics::~DialogStatistics()
{
    delete ui;
}
