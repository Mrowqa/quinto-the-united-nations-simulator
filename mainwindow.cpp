#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QStandardItem>
#include <QFile>
#include <QTextStream>
#include "dialogstatistics.h"
#include "dialogvotingsresults.h"
#include "dialogabout.h"

void MainWindow::askAboutSavingStatisticsIfNeeded(QString reason)
{
    if(nManager.getAllVotingsCount() > 0)
    {
        if(QMessageBox::question(this, "Save actual statistics", reason + " will cause lose actual statistics. Do you want to save it?")
                == QMessageBox::StandardButton::Yes)
                    onSaveStatistics();
    }
}

void MainWindow::onLoadTimesheet()
{
    askAboutSavingStatisticsIfNeeded("Loading new presence sheet");

    QString filename = QFileDialog::getOpenFileName(this, tr("Load presence list from file"),
        "..", "Text files (*.txt);;All files (*.*)");
    if(filename.isEmpty())
        return;

    std::size_t oldLocksAbleToDeleteCount = nManager.locksAbleToDelete;
    nManager.locksAbleToDelete = nManager.isActivedSession();
    if(!nManager.loadListFromFile(filename.toStdString()))
    {
        QMessageBox::critical(this, tr("Cannot load presence sheet"),
            tr("File doesn't exist or contains incorrect data! File format is described in ReadMe!.txt."));
        nManager.locksAbleToDelete = oldLocksAbleToDeleteCount;
        return;
    }
    bool canUnlock = nManager.endSession();
    nManager.locksAbleToDelete = oldLocksAbleToDeleteCount - canUnlock;
    nManager.resetStatistics();

    QStandardItemModel* iStandardModel = new QStandardItemModel(ui->nationsList);
    iStandardModel->clear();
    for(std::size_t i=0; i<nManager.getCount(); i++)
    {
        const Nation& nation = nManager[i];
        QStandardItem* item = new QStandardItem(nation.flag, nation.name.c_str());
        iStandardModel->appendRow(item);
    }
    ui->nationsList->setIconSize(QSize(32,20));
    ui->nationsList->setModel(iStandardModel);
    connect(ui->nationsList->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(onNationSelected(QItemSelection)));

    prepareUi(UiState::LoadedTimesheet);
    state = State::Other;
}

void MainWindow::onNewSession()
{
    if(!nManager.startSession())
        return;
    prepareUi(UiState::StartedSession);
    filterNationList(FilterListOptions::ShowAll);

    state = State::CheckingPresence;
    actualItem = -1;
    prepareNextItem();
}

void MainWindow::onNewVoting()
{
    if(!nManager.getSession().startVoting())
        return;
    prepareUi(UiState::StartedVoting);
    filterNationList(FilterListOptions::ShowPresent);

    state = State::Voting;
    actualItem = -1;
    prepareNextItem();
}

void MainWindow::filterNationList(FilterListOptions filterOptions)
{
    QStandardItemModel* iStandardModel = dynamic_cast<QStandardItemModel*>(ui->nationsList->model());
    if(!iStandardModel)
        return;

    switch(filterOptions)
    {
    case FilterListOptions::ShowAll:
        {
            pendingItems.clear();
            for(std::size_t i=0; i<nManager.getCount(); i++)
            {
                ui->nationsList->setRowHidden(i, false);
                iStandardModel->item(i)->setEnabled(true);
                pendingItems.insert(i);
            }
        }
        break;

    case FilterListOptions::ShowAllWithDisabledAbsent:
        {
            pendingItems.clear();
            for(std::size_t i=0; i<nManager.getCount(); i++)
            {
                ui->nationsList->setRowHidden(i, false);
                bool rowEnabled = true;
                try
                    { rowEnabled = nManager.getSession().isRegistered(i); }
                catch(std::logic_error)
                    { rowEnabled = true; }
                iStandardModel->item(i)->setEnabled(rowEnabled);
                pendingItems.insert(i);
            }
        }
        break;

    case FilterListOptions::ShowPresent:
        {
            for(std::size_t i=0; i<nManager.getCount(); i++)
                ui->nationsList->setRowHidden(i, true);

            pendingItems.clear();
            std::vector<std::size_t> present = nManager.getSession().getRegistered();
            for(auto it : present)
            {
                ui->nationsList->setRowHidden(it, false);
                iStandardModel->item(it)->setEnabled(true);
                pendingItems.insert(it);
            }
        }
        break;
    }
}

void MainWindow::onButton1Pushed()
{
    switch(state)
    {
    case State::CheckingPresence:
        if(nManager.getSession().setPresenceState(actualItem, NationSession::PresenceState::Present))
            prepareNextItem();
        break;

    case State::Voting:
        if(nManager.getSession().getVoting().vote(actualItem, NationVoting::Vote::InFavour))
            prepareNextItem();
        break;

    default:
        break;
    }
}

void MainWindow::onButton2Pushed()
{
    switch(state)
    {
    case State::CheckingPresence:
        if(nManager.getSession().setPresenceState(actualItem, NationSession::PresenceState::PresentAndVoting))
            prepareNextItem();
        break;

    case State::Voting:
        if(nManager.getSession().getVoting().vote(actualItem, NationVoting::Vote::Against)) // -------------------------- CHECKING IF VETO -----------------
            prepareNextItem();
        break;

    default:
        break;
    }
}

void MainWindow::onButton3Pushed()
{
    switch(state)
    {
    case State::CheckingPresence:
        if(nManager.getSession().setPresenceState(actualItem, NationSession::PresenceState::Absent))
            prepareNextItem();
        break;

    case State::Voting:
        if(nManager.getSession().getVoting().vote(actualItem, NationVoting::Vote::Abstain))
            prepareNextItem();
        break;

    default:
        break;
    }
}

void MainWindow::onShowStatistics()
{
    DialogStatistics dialogStatistics;
    dialogStatistics.setInfo(nManager);
    dialogStatistics.exec();
}

void MainWindow::onSaveStatistics()
{
    QString filename = QFileDialog::getSaveFileName(this, tr("Save statistics to file"),
        "", "Text files (*.txt);;All files (*.*)");
    if(filename.isEmpty())
        return;

    QFile file(filename);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox::critical(this, tr("Cannot save statistics"),
            tr("Cannot save statistics, cause application could not open file for writing.\nCheck whether you can create file and write in given location!"));
        return;
    }

    QString fileContent = prepareStatistics();
    QTextStream out(&file);
    out << fileContent;
    file.close();
}

void MainWindow::onAbout()
{
    DialogAbout dialogAbout;
    dialogAbout.exec();
    /*QString title, message;
    title = "About";
    message = "Quinto " + QString(QUINTO_VERSION) + "\n"
            + "\n\n--------- [About application] -------------\n"
            + "Quinto is a simple application for simulating votings.\n"
            + "Nation list can be loaded only from file, voting can be started\n"
            + "after checking presence, so after starting session. You can\n"
            + "select nations on the list by left.\n"
            + "-- For further informations see ReadMe!.txt\n"
            + "\n\n--------- [About author] -------------\n"
            + "Application is written by one young polish programmer.\n"
            + "Due to short developing period (some days) application is\n"
            + "underdeveloped (that's why this's alpha version). If you\n"
            + "want to I improve Quinto, feel free, just contact me.\n"
            + "I won't develop it if no one uses it.\n\n\n"
            + "Artur Jamro (Mrowqa)\n"
            + "artur.jamro@gmail.com\n"
            + "www.mrowqa.pl";
    QMessageBox::information(this, title, message);*/
}

void MainWindow::onNationSelected(const QItemSelection &selection)
{
    QList<QModelIndex> indexList = selection.indexes();
    if(!indexList.isEmpty())
    {
        actualItem = indexList.takeFirst().row();
        prepareNextItem(true);
    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->versionLabel->setText(QUINTO_VERSION);
    prepareUi(UiState::WaitingForPresenceList);
}

MainWindow::~MainWindow()
{
    askAboutSavingStatisticsIfNeeded("Exiting application");
    delete ui;
}

void MainWindow::prepareUi(MainWindow::UiState state)
{
    switch(state)
    {
    case UiState::WaitingForPresenceList:
        ui->actionLoad_Timesheet->setEnabled(true);
        ui->actionNew_Session->setEnabled(false);
        ui->actionNew_Voting->setEnabled(false);
        ui->actionSave_Statistics->setEnabled(false);
        ui->actionShow_Statistics->setEnabled(false);
        ui->statusInfo->setText("Waiting for presence list");
        ui->nationImage->setPixmap(QPixmap());
        ui->nationName->setText("");
        ui->button1->setHidden(true);
        ui->button2->setHidden(true);
        ui->button3->setHidden(true);
        break;

    case UiState::LoadedTimesheet:
        ui->actionLoad_Timesheet->setEnabled(true);
        ui->actionNew_Session->setEnabled(true);
        ui->actionNew_Voting->setEnabled(false);
        ui->actionSave_Statistics->setEnabled(true);
        ui->actionShow_Statistics->setEnabled(true);
        ui->statusInfo->setText("You can start new session");
        ui->nationImage->setPixmap(QPixmap());
        ui->nationName->setText("");
        ui->button1->setHidden(true);
        ui->button2->setHidden(true);
        ui->button3->setHidden(true);
        break;

    case UiState::StartedSession:
        ui->actionLoad_Timesheet->setEnabled(false);
        ui->actionNew_Session->setEnabled(false);
        ui->actionNew_Voting->setEnabled(false);
        ui->actionSave_Statistics->setEnabled(true);
        ui->actionShow_Statistics->setEnabled(true);
        ui->statusInfo->setText("Checking presence");
        ui->nationImage->setPixmap(QPixmap());
        ui->nationName->setText("");
        ui->button1->setVisible(true);
        ui->button2->setVisible(true);
        ui->button3->setVisible(true);
        ui->button1->setText("Present");
        ui->button2->setText("Present and voting");
        ui->button3->setText("Absent");
        break;

    case UiState::CheckedPresence:
    case UiState::VotingEnded:
        ui->actionLoad_Timesheet->setEnabled(true);
        ui->actionNew_Session->setEnabled(true);
        ui->actionNew_Voting->setEnabled(true);
        ui->actionSave_Statistics->setEnabled(true);
        ui->actionShow_Statistics->setEnabled(true);
        ui->statusInfo->setText("You can start new voting");
        ui->nationImage->setPixmap(QPixmap());
        ui->nationName->setText("");
        ui->button1->setHidden(true);
        ui->button2->setHidden(true);
        ui->button3->setHidden(true);
        break;

    case UiState::StartedVoting:
        ui->actionLoad_Timesheet->setEnabled(false);
        ui->actionNew_Session->setEnabled(false);
        ui->actionNew_Voting->setEnabled(false);
        ui->actionSave_Statistics->setEnabled(true);
        ui->actionShow_Statistics->setEnabled(true);
        ui->statusInfo->setText("Voting");
        ui->nationImage->setPixmap(QPixmap());
        ui->nationName->setText("");
        ui->button1->setVisible(true);
        ui->button2->setVisible(true);
        ui->button3->setVisible(true);
        ui->button1->setText("In favour");
        ui->button2->setText("Against");
        ui->button3->setText("Abstain");
        break;
    }
}

void MainWindow::prepareNextItem(bool causedByChangedListSelection)
{
    QStandardItemModel* iStandardModel = dynamic_cast<QStandardItemModel*>(ui->nationsList->model());
    if(!causedByChangedListSelection && actualItem != UINT(-1))
    {
        pendingItems.erase(actualItem);
        if(iStandardModel)
            iStandardModel->item(actualItem)->setEnabled(false);
    }

    if(pendingItems.empty() || nManager.getSession().isIdle())
    {
        switch(state)
        {
        case State::CheckingPresence:
            onCheckedPresence();
            break;

        case State::Voting:
            onVotingEnded();
            break;

        default:
            break;
        }
    }
    else
    {
        if(!causedByChangedListSelection)
        {
            auto nextItem = pendingItems.lower_bound(actualItem); // (unsigned) -1 forces ::end() iterator
            actualItem = nextItem==pendingItems.end() ? *pendingItems.begin() : *nextItem;
        }
        displayNationInfo();

        if(!causedByChangedListSelection)
        {
            QItemSelectionModel* iSelectionModel = ui->nationsList->selectionModel();
            QModelIndex iModel = iStandardModel->item(actualItem)->index();
            iSelectionModel->setCurrentIndex(iModel, QItemSelectionModel::ClearAndSelect);
        }

        ui->button3->setEnabled(state!=State::Voting || nManager.getSession().getPresenceState(actualItem)!=NationSession::PresenceState::PresentAndVoting);
    }
}

void MainWindow::displayNationInfo()
{
    std::size_t nationNumber = actualItem; // for easy change this value by argument
    ui->nationName->setText(nManager[nationNumber].name.c_str());
    ui->nationImage->setPixmap(nManager[nationNumber].flag.pixmap(250,150));
}

QString MainWindow::prepareStatistics()
{
    NationSession::Result result = nManager.getResult();

    std::size_t successCnt       = result[NationVoting::Result::Type::Success];
    std::size_t failureCnt       = result[NationVoting::Result::Type::Failure];
    std::size_t failureByVetoCnt = result[NationVoting::Result::Type::FailureVeto];
    std::size_t undecidedCnt     = result[NationVoting::Result::Type::Undecided];
    std::size_t allCount         = result[NationVoting::Result::Type::AllResults];

    double successPercent       = 100.0 * successCnt / allCount;
    double failurePercent       = 100.0 * (failureCnt + failureByVetoCnt) / allCount;
    double failureByVetoPercent = 100.0 * failureByVetoCnt / allCount;
    double undecidedPercent     = 100.0 * undecidedCnt / allCount;

    QString report;
    report = "\tSTATISTICS\n";
    if(allCount == 0)
        report += "\nNo passed votings!";
    else
    {
        report += "from " + QString::number(nManager.getStartedSessionsCount()) + " session with " + QString::number(nManager.getAllVotingsCount()) + " votings\n\n";
        report += "Successes: " + QString::number(successCnt) + " (" + QString::number(successPercent, 'f', 2) + "%)\n";
        report += "Failures: " + QString::number(failureCnt+failureByVetoCnt) + " (" + QString::number(failurePercent, 'f', 2) + "%)\n";
        report += "-- includes by veto: " + QString::number(failureByVetoCnt) + " (" + QString::number(failureByVetoPercent, 'f', 2) + "%)\n";
        report += "Undecided: " + QString::number(undecidedCnt) + " (" + QString::number(undecidedPercent, 'f', 2) + "%)";
    }

    return report;
}

void MainWindow::onCheckedPresence()
{
    state = State::Other;
    prepareUi(UiState::CheckedPresence);
    filterNationList(FilterListOptions::ShowAllWithDisabledAbsent);

    std::size_t presentCount = nManager.getSession().getRegisteredCount();
    std::size_t allCount = nManager.getCount();
    double presencePercent = 100.0 * presentCount / allCount;
    QString title, message;
    title = "Session " + QString::number(nManager.getStartedSessionsCount());
    message = "Present nations: " + QString::number(presentCount) + " of " + QString::number(allCount) + " (" + QString::number(presencePercent, 'f', 2) + "%).";
    if(presentCount == 0)
    {
        message = "No nation present!";
        ui->actionNew_Voting->setEnabled(false);
    }
    QMessageBox::information(this, title, message);
}

void MainWindow::onVotingEnded()
{
    state = State::Other;
    prepareUi(UiState::VotingEnded);
    filterNationList(FilterListOptions::ShowAllWithDisabledAbsent);

    DialogVotingsResults dialogVotingsResults(this);
    dialogVotingsResults.setInfo(nManager);
    dialogVotingsResults.exec();
}
