#include "mainwindow.h"
#include <QApplication>
#include <QMessageBox>

int main(int argc, char *argv[])
try
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
catch(std::exception ex)
{
    QMessageBox::critical(nullptr, "Runtime error", ex.what());
    return 1;
}
