#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "nationmanager.h"
#include <set>
#include <QListView>

#define QUINTO_VERSION "v0.8.2 Alpha"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public slots:
    void onLoadTimesheet();
    void onNewSession();
    void onNewVoting();
    void onButton1Pushed();
    void onButton2Pushed();
    void onButton3Pushed();
    void onShowStatistics();
    void onSaveStatistics();
    void onAbout();

private slots:
    void onNationSelected(const QItemSelection& selection);
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    NationManager nManager;
    std::set<std::size_t> pendingItems;
    std::size_t actualItem = -1;

    enum class State {
        CheckingPresence,
        Voting,
        Other
    } state = State::Other;

    enum class UiState {
        WaitingForPresenceList,
        LoadedTimesheet,
        StartedSession,
        CheckedPresence,
        StartedVoting,
        VotingEnded
    };

    enum class FilterListOptions {
        ShowAll,
        ShowAllWithDisabledAbsent,
        ShowPresent
    };

    void prepareUi(UiState state);
    void prepareNextItem(bool causedByChangedListSelection = false);
    void displayNationInfo();
    QString prepareStatistics();
    void filterNationList(FilterListOptions filter);
    void askAboutSavingStatisticsIfNeeded(QString reason);

    void onCheckedPresence();
    void onVotingEnded();
};

#endif // MAINWINDOW_H
