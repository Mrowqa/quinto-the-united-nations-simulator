#include "nationvoting.h"
#include "nationsession.h"
#include "nationmanager.h"


NationVoting::NationVoting(const NationSession& nSession) : session(nSession)
{
    assert(session.presenceSheet.size());

    votes.clear();
    votes.resize(session.presenceSheet.size(), NationVoting::Vote::AllVotes);
}

bool NationVoting::vote(std::size_t nationNumber, NationVoting::Vote vote)
{
    assert(nationNumber < votes.size());
    assert(vote != NationVoting::Vote::AllVotes);
    if(votes[nationNumber] != NationVoting::Vote::AllVotes)
        return false;

    if(session.presenceSheet[nationNumber] == NationSession::PresenceState::Absent)
        return false;
    if(vote == Vote::Abstain && session.presenceSheet[nationNumber] == NationSession::PresenceState::PresentAndVoting)
        return false;

    result.votes[UINT(vote)]++;
    if(vote == Vote::Against && session.nationManager[nationNumber].canVeto)
    {
        result.state = Result::Type::FailureVeto;
        result.votes[UINT(Vote::InFavour)] = result.votes[UINT(Vote::Against)] = result.votes[UINT(Vote::Abstain)] = 0;
    }
    else if(result[Vote::AllVotes] == session.getRegisteredCount())
    {
        if      (result[Vote::InFavour] > result[Vote::Against])    result.state = Result::Type::Success;
        else if (result[Vote::InFavour] < result[Vote::Against])    result.state = Result::Type::Failure;
        else                                                        result.state = Result::Type::Undecided;
    }

    return true;
}
