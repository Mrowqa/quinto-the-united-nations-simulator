#ifndef DIALOGVOTINGSRESULTS_H
#define DIALOGVOTINGSRESULTS_H

#include <QDialog>
#include "nationmanager.h"

namespace Ui {
class DialogVotingsResults;
}

class DialogVotingsResults : public QDialog
{
    Q_OBJECT

public:
    explicit DialogVotingsResults(QWidget *parent = 0);
    void setInfo(const NationManager& nManager);
    ~DialogVotingsResults();

private:
    Ui::DialogVotingsResults *ui;
};

#endif // DIALOGVOTINGSRESULTS_H
